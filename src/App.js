import { useEffect, useState } from 'react'
import "./App.css";
import SearchIcon from './search.svg';
import MovieCard from './MovieCard';
import { fireEvent } from '@testing-library/react';

//28acf197
// Static varaible
const API_URL = "http://www.omdbapi.com?apikey=28acf197";

const movie1 = {
  "Title": "Batman v Superman: Dawn of Justice",
  "Year": "2016",
  "imdbID": "tt2975590",
  "Type": "movie",
  "Poster": "https://m.media-amazon.com/images/M/MV5BYThjYzcyYzItNTVjNy00NDk0LTgwMWQtYjMwNmNlNWJhMzMyXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg"
}


function App() {
  //Define the movie api dynamic
  const [movies, setMovies] = useState([]);
  // Define the searhItme 
  const [searhItem, setSearhItem] = useState('');

  const serchMovie = async (title) => {
    const response = await fetch(`${API_URL}&s=${title}`)
    const data = await response.json();
    console.log(data);
    setMovies(data.Search);

  }

  useEffect(() => {
    serchMovie('Superman')
  }, []);

  return (
    <div className="app">
      <h1>Movieland</h1>

      <div className="search">
        <input
          placeholder="Search For movies"
          value={searhItem}
          onChange={(e) => setSearhItem(e.target.value)}
        />
        <img src={SearchIcon}
          alt="search"
          onClick={() => serchMovie(searhItem)}
        />
      </div>

      {
        movies?.length > 0 ? (<div className="container" >
          {movies.map((movie) => (
            <MovieCard key={movie.imdbID
            } movie={movie} />

          ))}
        </div>
        ) :
          (<div className="empty">
            <h2>No movies Found</h2>
          </div>)

      }



    </div>

  );
}

export default App;
